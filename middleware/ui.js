export default function({ route, store }) {
	// Take the last value (latest route child)
	const backButtonConfig = route.meta.reduce(
		(backButton, meta) => meta.backButton || backButton,
		{ shown: true, previousPath: '' }
	)

	store.commit('ui/UPDATE_BACK_BUTTON_CONFIG', backButtonConfig)
}
