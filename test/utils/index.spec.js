/* eslint-disable max-nested-callbacks */
import { formatSeconds } from '~/utils/index'

describe('utils', () => {
	describe('formatSeconds', () => {
		it('throws on invalid argument', () => {
			;['', [], {}, -1].forEach(arg => {
				expect(() => formatSeconds(arg)).toThrow()
			})
		})

		it('returns a string in the format mm:ss', () => {
			// eslint-disable-next-line prettier/prettier
			expect([0, 60, 90].map(formatSeconds))
				.toEqual(['00:00', '01:00', '01:30'])
		})
	})
})
