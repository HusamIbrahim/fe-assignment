import { shallowMount } from '@vue/test-utils'
import SearchingToast from '~/components/molecules/SearchingToast.vue'

describe('SearchingToast component', () => {
	it("emits a 'close' event when the close button is clicked", () => {
		const wrapper = shallowMount(SearchingToast)
		const button = wrapper.find('button')

		expect(wrapper.emitted().close).toBeFalsy()

		button.trigger('click')

		expect(wrapper.emitted().close).toBeTruthy()
		expect(wrapper.emitted().close.length).toBe(1)
	})

	it('is hidden when the close button is clicked', async () => {
		const wrapper = shallowMount(SearchingToast)
		const button = wrapper.find('button')

		expect(wrapper.vm.isShown).toBe(true)
		expect(wrapper.find('div').exists()).toBe(true)

		await button.trigger('click')

		expect(wrapper.vm.isShown).toBe(false)
		expect(wrapper.find('div').exists()).toBe(false)
	})
})
