/* eslint-disable max-nested-callbacks */
import { shallowMount } from '@vue/test-utils'
import AcceptMatchButton from '~/components/atoms/AcceptMatchButton.vue'

describe('AcceptMatchButton component', () => {
	it("emits an 'accept' event when clicked", () => {
		const wrapper = shallowMount(AcceptMatchButton)

		expect(wrapper.emitted().accept).toBeFalsy()

		wrapper.vm.$el.click()

		expect(wrapper.emitted().accept).toBeTruthy()
		expect(wrapper.emitted().accept.length).toBe(1)
	})

	it("automatically emits an 'accept' event after 1 second", () => {
		jest.useFakeTimers()
		const wrapper = shallowMount(AcceptMatchButton)

		expect(wrapper.emitted().accept).toBeFalsy()

		jest.advanceTimersByTime(60 * 1000)

		expect(wrapper.emitted().accept).toBeTruthy()
		expect(wrapper.emitted().accept.length).toBe(1)
	})
})
