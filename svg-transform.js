const compilerUtils = require('@vue/component-compiler-utils')
const VueTemplateCompiler = require('vue-template-compiler')

module.exports = {
	process(source) {
		const { render } = compilerUtils.compileTemplate({
			source,
			compiler: VueTemplateCompiler,
			isFunctional: false
		})

		return `module.exports = { render: ${render} }`
	}
}
