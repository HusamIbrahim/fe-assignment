export const state = () => ({
	selectedGame: {
		id: 'fifa-19',
		title: 'Fifa 19',
		image: {
			normal: require('~/assets/images/fifa19-ronaldo-fg-lg.png'),
			large: require('~/assets/images/fifa19-ronaldo-fg-lg@2x.png'),
			largest: require('~/assets/images/fifa19-ronaldo-fg-lg@3x.png')
		}
	}
})
