export const state = () => ({
	backButton: {
		shown: true,
		previousPath: ''
	}
})

export const mutations = {
	UPDATE_BACK_BUTTON_CONFIG(state, val) {
		state.backButton = { ...state.backButton, ...val }
	}
}
