/**
 * The index store is used for global settings, es: language
 */

export const state = () => ({
	pageTitle: ''
})

export const getters = {}

export const mutations = {
	UPDATE_PAGE_TITLE: (state, val) => {
		state.pageTitle = val
	}
}

export const actions = {}
