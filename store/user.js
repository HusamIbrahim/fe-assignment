export const state = () => ({
	profile: {
		username: 'steveroesler',
		points: 74,
		avatar: {
			small: require('~/assets/images/gf-avatar-01.png'),
			medium: require('~/assets/images/gf-avatar-01@2x.png'),
			large: require('~/assets/images/gf-avatar-01@3x.png')
		}
	}
})
