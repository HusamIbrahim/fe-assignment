/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
	mode: 'jit',
	theme: {
		fontFamily: {
			'akzidenz-grotesk-light': ['Akzidenz-Grotesk-Light', 'sans-serif'],
			'akzidenz-grotesk': ['Akzidenz-Grotesk', 'sans-serif'],
			'akzidenz-grotesk-medium': [
				'Akzidenz-Grotesk-Medium',
				'sans-serif'
			],
			'akzidenz-grotesk-bold': ['Akzidenz-Grotesk-Bold', 'sans-serif']
		},
		extend: {
			colors: {
				'gfinity-red': '#E94235',
				'electric-blue': '#293894',
				'dark-grey': '#1B1B1B',
				'light-grey': '#6B6B65',
				concrete: '#CFD1C7',
				sand: '#DFE0D9'
			}
		}
	},
	variants: {},
	plugins: []
}
