import { mapMutations } from 'vuex'

export default {
	mounted() {
		const titleChunk = this.$meta().refresh().metaInfo.titleChunk
		this.updatePageTitle(titleChunk)
	},
	methods: {
		...mapMutations({
			updatePageTitle: 'UPDATE_PAGE_TITLE'
		})
	}
}
