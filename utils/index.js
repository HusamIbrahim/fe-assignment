/**
 * @param {number} seconds
 *
 * @returns {string} A formatted string in the format "mm:ss"
 */
export function formatSeconds(seconds) {
	if (!Number.isFinite(seconds) || seconds < 0) {
		throw new Error('Invalid argument')
	}

	const minutes = seconds / 60
	const mm = Math.floor(minutes)
	const ss = Math.round((minutes % 1) * 60)

	// eslint-disable-next-line prettier/prettier
	return `${mm.toString().padStart(2, '0')}:${ss.toString().padStart(2, '0')}`
}
