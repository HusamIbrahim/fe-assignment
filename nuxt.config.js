module.exports = {
	mode: 'universal',
	/*
	 ** Headers of the page
	 */
	head: {
		titleTemplate: 'Gfinity Esports | %s',
		meta: [
			{ charset: 'utf-8' },
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1'
			},
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || ''
			}
		],
		link: [
			{ rel: 'icon', type: 'image/png', href: '/favicon.png' },
			{
				rel: 'preload',
				as: 'font',
				type: 'font/ttf',
				href: '/fonts/akzidenzgrotesk-light-webfont.ttf',
				crossorigin: 'true'
			},
			{
				rel: 'preload',
				as: 'font',
				type: 'font/ttf',
				href: '/fonts/akzidenzgrotesk-regular-webfont.ttf',
				crossorigin: 'true'
			},
			{
				rel: 'preload',
				as: 'font',
				type: 'font/ttf',
				href: '/fonts/akzidenzgrotesk-medium-webfont.ttf',
				crossorigin: 'true'
			},
			{
				rel: 'preload',
				as: 'font',
				type: 'font/ttf',
				href: '/fonts/akzidenzgrotesk-bold-webfont.ttf',
				crossorigin: 'true'
			}
		]
	},
	/*
	 ** Customize the progress-bar color
	 */
	loading: { color: '#fff' },
	/*
	 ** Global CSS
	 */
	css: [],
	/*
	 ** Plugins to load before mounting the App
	 */
	plugins: [],
	/*
	 ** Auto import components
	 ** See https://nuxtjs.org/api/configuration-components
	 */
	components: [
		'~/components', // shortcut to { path: '~/components' }
		{ path: '~/components/atoms/', prefix: 'atom' },
		{ path: '~/components/molecules/', prefix: 'mol' },
		{ path: '~/components/organisms/', prefix: 'org' }
	],
	/*
	 ** Nuxt.js dev-modules
	 */
	buildModules: [
		// Doc: https://github.com/nuxt-community/eslint-module
		'@nuxtjs/eslint-module',
		// Doc: https://github.com/nuxt-community/nuxt-tailwindcss
		'@nuxtjs/tailwindcss',
		'@nuxtjs/svg'
	],
	/*
	 ** Nuxt.js modules
	 */
	modules: [
		// Doc: https://axios.nuxtjs.org/usage
		'@nuxtjs/axios'
	],
	/*
	 ** Axios module configuration
	 ** See https://axios.nuxtjs.org/options
	 */
	axios: {},
	/*
	 ** Build configuration
	 */
	build: {
		/*
		 ** You can extend webpack config here
		 */
		extend(config, ctx) {},
		transplie: ['gsap']
	},

	router: {
		middleware: ['ui']
	}
}
